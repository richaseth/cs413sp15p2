******************************************TestPerformance.java**************************************************

TODO: run test and record running times for SIZE = 10, 100, 1000, 10000

Answer
size 10		0.225 sec
size 100:	0.279 sec
size 1000:	0.872 sec
size 10000: 	8 sec

TODO: which of the two lists performs better as the size increases?

Answer
linkedlist
10:	0.079
100	0.066
1000	0.057
10000	0.06

arraylist 
10	0.059
100	0.07 sec
1000	0.328 sec
10000	2 sec

Conclusion: As the size increases the linked list performs better however with smaller sizes arraylist works better
We can say when it comes to traversing arraylist performs better, whereas linked list is better if items are needed to picked up/remove from arbitrary location

********************************************TestIterator.java**********************************************************

TODO: also try with a LinkedList - does it make any difference?

Answer
arraylist	0.083
linkedlist	0.044

Arraylist takes little extra time as there are more add and remove operations in this file not much traversing.
TODO: express which values are left in the list 

Answer
[33, 44, 55, 66]

TODO: what happens if you use list.remove(77)?

Answer
index out of bound error on List.remove(77) as it searches 77th index

**************************************************TestList.java*********************************************************

TODO: also try with a LinkedList - does it make any difference?

Answer
arraylist	0.06
linkedlist	0.074

It would take almost same time as size is rather small but as the size increases the arraylist works better while traverse and linked list works better if we need to add or remove

TODO: list.remove(5); what does this method do?

Answer
It will remove the value from fifth index.

TODO: list.remove(Integer.valueOf(5)); what does this one do?

Answer
It will remove 5 from the list

